#!/bin/bash

# Updates the dotfiles in the dotfiles folder
updateRepoFiles() {
	cp ~/.tmux.conf ~/dotfiles/.tmux.conf
	cp ~/.vimrc ~/dotfiles/.vimrc
	cp ~/.bashrc ~/dotfiles/.bashrc
	cp ~/.bash_aliases ~/dotfiles/.bash_aliases
	cp -r ~/.vim ~/dotfiles

	# Print a confirmation message to the console
	echo "Repository files have been updated"
}

# Updates the dotfiles in the home directory
updateDotFiles() {
	cp ~/dotfiles/.tmux.conf ~/.tmux.conf
	cp ~/dotfiles/.vimrc ~/.vimrc
	cp ~/dotfiles/.bashrc ~/.bashrc
	cp ~/dotfiles/.bash_aliases ~/.bash_aliases
	cp -r ~/dotfiles/.vim ~/

	# Print a confirmation message to the console
	echo "Home directory dotfiles have been updated"
}

# Create backups of home directory dotfiles
createBackups() {

	# Check if backups directory exists.
	if [ ! -d ~/dotfiles/backups ] ;then
	
		# Create backup directory
		mkdir ~/dotfiles/backups

	fi

	# Copy home directory dotfiles to the backups directory
	cp ~/.tmux.conf ~/dotfiles/backups/.tmux.conf
	cp ~/.vimrc ~/dotfiles/backups/.vimrc
	cp ~/.bashrc ~/dotfiles/backups/.bashrc
	cp ~/.bash_aliases ~/dotfiles/backups/.bash_aliases
	cp -r ~/.vim ~/dotfiles/backups

}

# Recursive function to decide what action to perform
decideAction() {
	if [ "$action" == "1" ] ;then

		# Prompt user for desired action
		printf "WARNING! This will overwrite the contents of the files in the dotfiles directory. Do you wish to continue? [Y/n] "

		# Read users response from the prompt
		read -r answer

		if [ "$answer" == "Y" ] || [ "$answer" == "y" ] ;then

			# Update the dotfiles in the git repository folder
			updateRepoFiles

		elif [ "$answer" == "N" ] || [ "$answer" == "n" ] ;then

			# Print to screen that no operation was performed
			echo "No operation performed"


		else

			# Re-prompt user if response is not Y or N
			echo "Please answer Y for Yes or N for No"

			# Recursively call decideAction until a correct option is given
			decideAction

		fi

	elif [ "$action" == "2" ] ;then
	
		# Prompt user for desired action
		printf "WARNING! This will overwrite the contents of the files in the home directory. Do you wish to continue? [Y/n] "
	
		# Read users response from the prompt
		read -r answer

		if [ "$answer" == "Y" ] || [ "$answer" == "y" ] ;then

			# Create backups of the dotfiles in the home directory
			createBackups

			# Notify user that backups were created and where to find them
			echo "Backups of the home directory dotfiles were created and placed in ~/dotfiles/backups"

			# Update the dotfiles in the home directory
			updateDotFiles

		elif [ "$answer" == "N" ] || [ "$answer" == "n" ] ;then

			# Print to screen that no operation was performed
			echo "No operation performed"

		else

			# Re-prompt user if response is not Y or N
			echo "Please answer Y for Yes or N for No"

			# Recursively call decideAction until a correct options is given
			decideAction

		fi

	elif [ "$action" == "3" ] ;then

		# Print to screen that the operation is being cancelled
		echo "Cancelling operation"

	fi

}

# Print actions
echo "1. Copy home directory dotfiles to the git repository dotfiles folder
2. Copy git repository dotfiles to home directory dotfiles
3. Cancel"

# Prompt user for desired action
printf "Which action would you like to perform? "

# Read the users response
read -r action

# Re-prompt user for action until they answer 1, 2, or 3
until [[ "$action" == "1" ]] || [[ "$action" == "2" ]] || [[ "$action" == "3" ]] ;do

	# Prompt user for an action
	printf "Please answer with option 1, 2, or 3: "

	# Store users response in a variable
	read -r action

done

# Call decideAction to run correct operation
decideAction
