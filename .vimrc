" Set line numbers
set number

" Set text wrapper
" set textwidth=79

" Highlight search
set hlsearch

" Start Pathogen Plugin Manager
set laststatus=2
execute pathogen#infect()
syntax on
filetype plugin indent on

let g:lightline = {
  \ 'colorscheme': 'onedark',
  \ 'separator': { 'left': '', 'right': '' },
  \ 'subseparator': { 'left': '', 'right': '' }}

" Remove VIM status line
" silent e
" set noshowmode

"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
""If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
	if (has("nvim"))
		"For Neovim 0.1.3 and 0.1.4 <https://github.com/neovim/neovim/pull/2198 >
		let $NVIM_TUI_ENABLE_TRUE_COLOR=1
        endif
        "For Neovim > 0.1.5 and Vim > patch 7.4.1799 <https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162>
        "Based on Vim patch 7.4.1770 (`guicolors` option) <https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd>
        " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
        if (has("termguicolors"))
		set termguicolors
        endif
endif

"onedark.vim override: Set a custom background color in the terminal
if (has("autocmd") && !has("gui_running"))
	augroup colors
      		autocmd!
        	let s:background = { "gui": "#282C34", "cterm": "16", "cterm16": "0" }
        	autocmd ColorScheme * call onedark#set_highlight("Normal", { "bg": s:background }) "No `fg` setting
	augroup END
endif

" Removes background color from vim lightline status bar
"let s:palette = g:lightline#colorscheme#{g:lightline.colorscheme}#palette
"let s:palette.normal.middle = [ [ 'NONE', 'NONE', 'NONE', 'NONE' ] ]
"let s:palette.inactive.middle = s:palette.normal.middle
"let s:palette.tabline.middle = s:palette.normal.middle

" OneDark Color Scheme
colorscheme onedark

" Changes tabs to spaces
set tabstop=4

" Set tab shifts to 4 spaces
set shiftwidth=4

" Change search highlight color
highlight link Searchlight Incsearch
hi Search cterm=NONE ctermfg=black ctermbg=green
hi IncSearch cterm=NONE ctermfg=black ctermbg=blue

" Set search forward/reverse to stay on current word
nnoremap * *``
nnoremap # #``

" File type plugin for NERD Commenter
filetype plugin on

" NERD Commenter
" Create default mappings
let g:NERDCreateDefaultMappings = 1

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Enable NERDCommenterToggle to check all selected lines is commented or not 
let g:NERDToggleCheckAllLines = 1

" " Set toggle for NERD Commenter
nmap <C-_> <Plug>NERDCommenterToggle<CR>
vmap <C-_> <Plug>NERDCommenterToggle<CR>gv

" " NERD Tree
" Set toggle for NERDTree
nnoremap <C-\> :NERDTreeToggle<CR>

" " Mouse features
" " Mouse Scrolling
" set mouse=a

" " Remap Mouse Clicks
" " Left Click
" imap <LeftMouse> <nop>
" vmap <LeftMouse> <nop>

" " Double Left Click
" nmap <2-LeftMouse> <nop>
" imap <2-LeftMouse> <nop>
" vmap <2-LeftMouse> <nop>

" " Right Click
" nmap <RightMouse> <nop>
" imap <RightMouse> <nop>
" vmap <RightMouse> <nop>

