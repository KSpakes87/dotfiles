alias ll='ls -alFh'
alias lsa='ls -A'
alias l='ls -CF'
alias ls='ls --color=auto'
alias cdrtbh='cd /mnt/c/Users/kww/Documents/BHR/blocking/rtbh-python/rtbh_python'
alias cdinfr='cd /mnt/c/Users/kww/Documents/AI-ATAC/infrastructure'
alias server-room-temp='ssh -o LogLevel=QUIET -t impetus "sudo /usr/local/bin/pcsensor"'
alias vimr='(){vim scp://${1}/${2};}'
alias firefox='/mnt/c/Program\ Files/Mozilla\ Firefox/firefox.exe'
alias ap='ansible-playbook'

# Mount Z: drive
alias mntz='sudo mount -t drvfs Z: /mnt/z'

# Display a graph for git working tree
alias graph='git log --all --decorate --oneline --graph'

# Change to Windows Terminal profile location
alias windows-terminal='cd /mnt/c/Users/KSpak/AppData/Local/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState'
