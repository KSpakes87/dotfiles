# DOTFILES

### .bashrc

Makes a custom command line that mimics powerline

### .tmux.conf

Styles the tmux status bar like powerline

### .vimrc

Theme - Atom One Dark  
Status Line - Powerline

### dotfiles_updater.sh

Copies the dotfiles from the git repo folder to the users home dotfiles or from their home dotfiles to the git repo folder.
Basically, this script makes it easier to move your new configurations over to the git repository folder. You can then
push to Gitlab or Github. Next, you will be able to pull down your dotfiles configurations onto another computer and easily
update the home directory dotfiles with your personal dotfiles you have already created.

#### WARNING!
Be very careful using this script. It will overwrite your dotfiles. I have put in warnings to give you a chance to cancel if
you accidentally picked the wrong option or change your mind.

##### Future Updates

  * Give user the option to only update specified dotfiles instead of all of them.
